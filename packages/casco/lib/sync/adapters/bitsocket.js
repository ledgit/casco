const axios = require("axios");
const EventSource = require("eventsource");
const es = require("event-stream");
const Events = require("events");
const Validator = require("fastest-validator");

const endpoints = {
  query: "https://txo.bitbus.network/block",
  socket: "https://txo.bitsocket.network/s/",
};

const client = axios.create();
client.defaults.headers.common.token =
  "eyJhbGciOiJFUzI1NksiLCJ0eXAiOiJKV1QifQ.eyJzdWIiOiIxNTYxRVBrcEphYXFKWURMUDdlOHVCb2tROW5UTGdiV2hNIiwiaXNzdWVyIjoiZ2VuZXJpYy1iaXRhdXRoIn0.SUsvSGNHT0pjclZqUFVzekRwaFN4QUJCMHBpNnZaZ21jV2d6bXluMWk5UXJaZ01mU1M0U2MyWmtzT0UvOGhNZ2RBSGcrSUxad2tZOGU4R3FxM000c0tJPQ";

if (typeof btoa !== "function") {
  function btoa(str) {
    return Buffer.from(str.toString(), "binary").toString("base64");
  }
}

const validator = new Validator();
const schemas = {
  bitDbResponseSchema: {
    tx: "object",
    in: "array",
    out: "array",
    blk: { type: "object", optional: true }
  }
};
const isValidBitDbResponse = validator.compile(schemas.bitDbResponseSchema);



const bitdb = {
  queries: {
    socket: {
      bit(address) {
        return {
          q: {
            find: {
              "out.s2": address,
            },
          },
        };
      },

      paymentTo(address) {
        return {
          v: 3,
          q: {
            find: { "out.e.a": address },
          },
        };
      },

      paymentFrom(address) {
        return {
          v: 3,
          q: {
            find: { "in.e.a": address },
          },
        };
      },
    },

    db: {
      bit(address, height) {
        height = height || 0;
        return {
          q: {
            find: {
              "blk.i": { $gt: height },
              "out.s2": address,
            },
            sort: { "blk.i": 1 },
          },
        };
      },

      paymentTo(address, height) {
        height = height || 0;
        return {
          v: 3,
          q: {
            find: {
              "out.e.a": address,
              $or: [
                { "blk.i": { $gt: height } },
                { "blk.i": { $exists: false } },
              ],
            },
          },
        };
      },

      paymentFrom(address, height) {
        height = height || 0;
        return {
          v: 3,
          q: {
            find: {
              "in.e.a": address,
              $or: [
                { "blk.i": { $gt: height } },
                { "blk.i": { $exists: false } },
              ],
            },
          },
        };
      },
    },
  },

  socket(query) {
    const url = endpoints.socket + btoa(JSON.stringify(query));

    const eventSource = new EventSource(url);
    const events = new Events();

    eventSource.onmessage = (message) => {
      let data;
      try {
        data = JSON.parse(message.data);
      } catch (e) {
        console.error(e);
        return;
      }

      if (data.type == "open") {
        return;
      }

      let bitDbResponse = data.data[0];
      events.emit("tx", bitDbResponse);
    };

    return events;
  },

  db(query) {
    const events = new Events();

    client({
      url: endpoints.query,
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        token:
          "eyJhbGciOiJFUzI1NksiLCJ0eXAiOiJKV1QifQ.eyJzdWIiOiIxNTYxRVBrcEphYXFKWURMUDdlOHVCb2tROW5UTGdiV2hNIiwiaXNzdWVyIjoiZ2VuZXJpYy1iaXRhdXRoIn0.SUsvSGNHT0pjclZqUFVzekRwaFN4QUJCMHBpNnZaZ21jV2d6bXluMWk5UXJaZ01mU1M0U2MyWmtzT0UvOGhNZ2RBSGcrSUxad2tZOGU4R3FxM000c0tJPQ",
      },
      data: query,
      responseType: "stream",
    })
      .then((r) => r.data)
      .then((r) => {
        r.on('end', () => {
          events.emit('end');
        })
        r.pipe(es.split()).pipe(
          es.mapSync((t) => {
            if (t) {
              let j = JSON.parse(t);
              events.emit("tx", j);
            }
          })
        );
      })

    return events;
  },
};

module.exports = bitdb;
