const CircularBuffer = require("circular-buffer");
const Queue = require("queue");
const adapter = require("./adapters/bitsocket");

// const snooze = ms => new Promise(resolve => setTimeout(resolve, ms));

function debug(str) {
  if (process.env.DEBUG) {
    console.log(str);
  }
}

module.exports = async function (app, socketQuery, dbQuery) {
  let syncing = true;

  // This will hold seen txid, to prevent any double dispatches when we switch from syncing mode(bitquery) to running mode (bitsocket)
  const seen = new CircularBuffer(25);

  // This paused queue will hold all tx's that are received during syncing(bitquery), after the bitquery txs are processed,
  // this queue will be put into autostart mode
  const queue = Queue({ concurrency: 1, autostart: false });

  const realtime = adapter.socket(socketQuery);
  realtime.on("tx", (t) => {
    if (syncing) {
      debug("tx while syncing! adding to queue");
    }

    queue.push(function (cb) {
      const isAlreadySeen = seen.toarray().indexOf(t.tx.h) >= 0;
      if (isAlreadySeen) {
        debug("TX", bitDbResponse.tx.h, "already seen! Doing nothing");
        cb();
      }
      debug("Now processing tx in the queue");
      dispatch(t);
      cb();
    });
  });

  const blocks = await adapter.db(dbQuery);
  blocks.on("tx", (t) => {
    dispatch(t);
    seen.enq(t.tx.h);
  });

  queue.on('start', () => {
    //
  });

  blocks.on("end", () => {
    debug("Queue length is ", queue.length)
    // process all transactions that came in while syncing from bitdb query
    queue.start(function (err) {
      if (err) {
        console.error("queue start error", err);
        process.exit();
      }
      queue.autostart = true;
      if (syncing) {
        debug("Syncing complete");
        syncing = false;
      }
    });
  });

  function dispatch(bitDbResponse) {
    let req = {};
    let res = {};

    req.tx = bitDbResponse;
    req.isSyncing = syncing;

    // Send through umr
    app.handle(req, {}, () => {});
  }

  return;
};
